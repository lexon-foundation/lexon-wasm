# lexon-wasm npm packages

## How to install

* npm install lexon-wasm

* import similar to `const lexon = import("lexon-wasm");`

## Usage

* compile function

Example code:

`let solidity = this.state.wasm.compile(this.state.lexon_contract);
    solidity = this.state.wasm.solidity()`
