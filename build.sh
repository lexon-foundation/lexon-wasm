#!/bin/sh
cargo build --release --target wasm32-unknown-unknown

# update the wasm bindings:
wasm-pack build --release
